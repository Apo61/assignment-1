from bs4 import BeautifulSoup
from requests.sessions import HTTPAdapter
import urllib3
import requests

html_page = requests.get("https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/Press-Release?fbclid=IwAR2kxmcitGhXiye7Xkui1j4fd3LA9zIzeA0YY7fBFH_1f0ly8g3mrKjfb9c")
html_image = urllib3.PoolManager()
url = "https://mofa.gov.bd/site/view/service_box_items/PRESS%20RELEASES/Press-Release?fbclid=IwAR2kxmcitGhXiye7Xkui1j4fd3LA9zIzeA0YY7fBFH_1f0ly8g3mrKjfb9c"
soup = BeautifulSoup(html_page.content, "html.parser")

# title of the page
title = soup.title
print(title)