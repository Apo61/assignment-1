from bs4 import BeautifulSoup

with open("scrapped_index.html", encoding='utf-8') as fp:
    soup = BeautifulSoup(fp, 'html.parser')
    
    title_box = soup.find('title')
    name = title_box.text.strip() # strip() is used to remove starting and trailing
    print(name)

    """finding all meta tags"""
    meta_box = soup.find_all('meta')
    print(meta_box)
    
    """finding  tags"""
    #anchor tags
    a_box=soup.find_all('a') 
    print(a_box)
    
    #input_box
    input_box=soup.find_all('input')
    print(input_box)


    #form_box
    form_box=soup.find('form')
    print(form_box.text.strip())

    #images
    images = []
for img in soup.find_all("img"):
    images.append(img.get("src"))
    print(images)





